﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TeacherPC
{
    static class SocketController
    {

        private static string ip
        {
            get
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
                return null;
            }
        }

        static IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(ip), 32768);

        static private int udpPort = 32767;

        static private UdpClient udpClient = new UdpClient();
        static private bool _isudpActive = false;
        public static bool isUDPActive
        {
            set { _isudpActive = !_isudpActive; }
            get { return _isudpActive; }
        }

        static private Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static private bool _istcpActive = false;
        public static bool isTCPActive
        {
            set { _istcpActive = !_istcpActive; }
            get { return _istcpActive; }
        }

        //Метод TCP сокета. Используется для отправки важных сообщений, для предотвращения потери данных во время пересылки.
        public static void TCPSocket()
        {
            
        }

        //Метод UDP сокета. Используется для отправки не совсем важных/статусных сообщений
        public static void UDPSocket()
        {
            
        }
        
        private static void TCPMessaging(string Message)
        {

        }

        public static void UDPSend()
        {

        }

        public static void UDPListen()
        {

        }

    }
}
