﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeacherPC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Server.isEnabled = true;

            ThreadController.TCPThreadStart();
            ThreadController.UDPThreadStart();

            Application.Run(new MainForm());

            ThreadController.TCPThreadAbort();
            ThreadController.UDPThreadAbort();
        }
    }
}
