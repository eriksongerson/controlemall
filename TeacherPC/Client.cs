﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherPC
{
    class Client
    {
        private string _ip;
        public string Ip
        {
            get { return _ip; }
        }

        string _name = ""; 
        public string Name
        {
            get { return _name; }
        }

        public Client(string name, string ip)
        {
            _name = name;
            _ip = ip;
        }

        ~Client()
        {
            _name = null;
            _ip = null;
        }

    }
}
