﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace TeacherPC
{
    static class ThreadController
    {

        static private Thread TCPThread = new Thread(new ThreadStart(SocketController.TCPSocket));
        static private Thread UDPThread = new Thread(new ThreadStart(SocketController.UDPSocket));

        static public void TCPThreadStart()
        {
            TCPThread.Start();
            while (!TCPThread.IsAlive) ;
        }

        static public void UDPThreadStart()
        {
            UDPThread.Start();
            while (!UDPThread.IsAlive) ;
        }

        static public void TCPThreadAbort()
        {
            TCPThread.Abort();
            while (TCPThread.IsAlive) ;
        }

        static public void UDPThreadAbort()
        {
            UDPThread.Abort();
            while (UDPThread.IsAlive) ;
        }

        private static void FuncUDPThread()
        {

            while(SocketController.isUDPActive == true)
            {

                

            }

        }

        private static void FuncTCPThread()
        {

            while(SocketController.isTCPActive == true)
            {



            }

        }

    }
}
